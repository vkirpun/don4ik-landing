import React from 'react';
import classnames from 'classnames';

import laptop from './images/laptop.png';
import project from './images/project.png';
import grid1 from './images/grid-image-1.png';
import grid2 from './images/grid-image-2.png';
import video from './images/video.png';
import play from './images/play.png';
import man from './images/man.jpg';
import wd from './images/wd.png';
import vk from './images/vk.png';
import eye from './images/eye.png';

import styles from './App.module.scss';

const randomInteger = (min, max) => {
  // случайное число от min до (max+1)
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
};

const App = () => (
  <>
    <header>
      <div className={styles.container}>
        <nav className={styles.nav}>
          <img src={wd} alt="wd logo" />

          <ul className={styles.navList}>
            <li className={styles.navItem}>Главная</li>
            <li className={styles.navItem}>Об авторе</li>
            <li className={styles.navItem}>Работы</li>
            <li className={styles.navItem}>Процесс</li>
            <li className={styles.navItem}>Контакты</li>
          </ul>
        </nav>

        <div className={styles.headerInfo}>
          <img src={laptop} alt="laptop" />

          <div>
            <h1>
              Дизайн и верстка
            </h1>

            <p>
              Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является
              стандартной "рыбой" для текстов на латинице с начала XVI века.
            </p>

            <button>
              Написать мне
            </button>
          </div>
        </div>
      </div>
    </header>

    <section className={classnames([styles.container, styles.aboutMe])}>
      <h2>Обо мне</h2>
      <p>
        Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной
        "рыбой" для текстов на латинице с начала XVI века.
      </p>
    </section>

    <section className={styles.projects}>
      <div className={styles.projectsContainer}>
        {Array(6).fill('').map((el, idx) => (
          <div className={styles.project} key={idx}>
            <img src={project} alt="project-icon" />

            <div>
              <h3>40+</h3>
              <p>проектов</p>
            </div>
          </div>
        ))}
      </div>
    </section>

    <section className={classnames([styles.container, styles.mySkills])}>
      <div>
        <h2>Мои навыки</h2>

        {Array(3).fill('').map((el, idx) => (
          <div className={styles.skill} key={idx}>
            <p>Adobe Photoshop</p>

            <div className={styles.bar}>
              <div className={styles.doneBar} />
            </div>
          </div>
        ))}
      </div>

      <img src={man} alt="man image" />
    </section>

    <section className={styles.video}>
      <div className={styles.container}>
        <h2>Как я работаю</h2>

        <p>
          Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной
          "рыбой" для текстов на латинице с начала XVI века.
        </p>

        <div>
          <img className={styles.videoImg} src={video} alt="video" />
          <img className={styles.play} src={play} alt="play" />
        </div>
      </div>
    </section>

    <section className={styles.grid}>
      {Array(8).fill('').map((el, idx) => {
        const imageNumber = randomInteger(0, 1);
        const image = imageNumber === 1 ? grid1 : grid2;

        return (
          <div key={idx}>
            <img src={image} alt="grid2" />
            <div className={styles.backdrop}>
              <img className={styles.eye} src={eye} alt="eye" />
            </div>
          </div>
        );
      })}
    </section>

    <section className={styles.form}>
      <div className={styles.container}>
        <h2>Хотите веб-сайт?</h2>
        <p>
          Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной
          "рыбой" для текстов на латинице с начала XVI века.
        </p>

        <form>
          <div>
            <input type="text" placeholder="Ваше имя" />
            <input type="text" placeholder="Ваш e-mail" />
          </div>
          <textarea placeholder="Сообщение" cols="30" rows="10" />
        </form>

        <button>Отправить</button>
      </div>
    </section>

    <footer>
      <div className={styles.container}>
        <div>
          <h3>Иванов Иван</h3>
          <span>(с) 2018. Все права защищены.</span>
        </div>

        <div className={styles.social}>
          <img src={vk} alt="vk" />
          <img src={vk} alt="vk" />
          <img src={vk} alt="vk" />
        </div>
      </div>
    </footer>
  </>
);

export default App;
